# Beep Christmas Initiative

### Products
[Xiaomi MiPlug](https://shopee.ph/Original-Xiaomi-Smart-Socket-Plug-Basic-Version-i.35435533.505699523) 

Notes: 
- You might need a xiaomi smart hub for it to connect.
- It brodcasts itself as long as it is plug. So even though it's turned off, you can still access it.


### How to use
1. Install all the needed dependencies in requirements.txt
2. run server.py 

### Documentation and References
[Xiaomi Openhab References and API](https://www.openhab.org/addons/bindings/mihome/#configuration-examples)
- Alternative open source api wrapper for controlling mi plug devices

[Xiaomi's MiHome Binary Protocol](https://github.com/OpenMiHome/mihome-binary-protocol)
- Contains various protocol on how the plugs send packets and messages to the client and the device. It defines access points and ways to decode packets from the device.

[npm lib for miio](https://www.npmjs.com/package/miio)
- Alternative library for JS

[Python Miio library and console tool for controlling Xiaomi Smart Appliances](https://github.com/rytilahti/python-miio)


###  Schematics
- See Diagram for Beep Christmas Tree PDF file.
- Alternative setup: Raspbery Pi Server and Arduino Wifi Chips for scaling


### Smart Plug Ideas 
- [DIY Smart Plug](https://www.hackster.io/taifur/smart-plug-b653f2)
- [Cheap DIY Arduino Wifi Setup](https://medium.com/@monkeytypewritr/amazon-echo-esp8266-iot-a42076daafa5)
- [Simple DIY Smart Socket Example](https://www.engineer2you.com/2017/12/make-smart-socket-wifi.html)

