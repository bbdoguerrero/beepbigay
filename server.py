from sanic import Sanic
from sanic.response import text, json
import os

app = Sanic()

# Builders
ip = "--ip 192.168.13.1 " #USUALLY THE SAME UNLESS ON THE ADAPTER
token = "--token adc3e8e681b6f87d8b2c99057217b66f " #CHANGE
command = "miplug "
status = " "

@app.route("/")
async def test(request):

    return text("SERVER CONNECTION SUCCESSFUL")

#TODO: check logs
@app.route("/api/switch")
async def switcher(request):
    if request.args.get('turn') == "on":
        onCommand = command + ip + token + "on"
        if os.system(onCommand):
            status = "SUCCESS"
            print("Turned on..")
        else:
            status = "ON FAIL"
            print("ON: An error occured..")
    elif request.args.get('turn') == "off":
       offCommand = command + ip + token + "off"
       if os.system(offCommand):
           status = "SUCCESS"
           print("Turned off..")
       else:
           status = "OFF FAIL"
           print("OFF: An error occured..")    
    else:
        status = "REQUEST FAIL"
        print("request error!")

    return json({"status": status})

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=3000)

